﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using ArabicSupport;

public class FixArabicText : Editor
{
    [MenuItem("Tools/Fix Arabic Text &%a")]
    public static void Fix()
    {
        for (int i = 0; i < Selection.gameObjects.Length; i++)
        {
            Text t = Selection.gameObjects[i].GetComponent<Text>();
            if (t != null)
            {
                Undo.RecordObject(t, "Fixing Arabic");
                t.text = ArabicFixer.Fix(t.text);
            }
        }
    }
}