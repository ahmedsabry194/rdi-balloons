﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Balloons_LetterManager : MonoBehaviour
{
    public List<Letter> Letters = new List<Letter>();
    private List<Letter> RandomLetters = new List<Letter>();
    public Letter SelectedLetter;
    public int requiredCorrectAnswers = 10;

    [SerializeField] private GameObject winPanel;
    [SerializeField] private GameObject winParticlePrefab;
    [SerializeField] private Image correctAnswersIndicator;
    [SerializeField] private Text letter_TXT;

    private int correctAnswers = 0;

    public Letter RandomLetter
    {
        get
        {
            int index = Random.Range(0, RandomLetters.Count);
            return (RandomLetters[index]);
        }
    }

    public static Balloons_LetterManager Instance;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);
    }

    private void Start()
    {
        this.SelectedLetter = Letters.Find(t => t.LetterChar == Balloons_LetterSelector.Instance.selectedLetter);
        letter_TXT.text = SelectedLetter.LetterChar;
        SetFiveRandomLetters();
    }

    private void SetFiveRandomLetters()
    {
        List<Letter> letters = new List<Letter>();
        letters.Add(SelectedLetter);

        for (int i = 0; i < 4; i++)
        {
            Letter let;

            do
            {
                int rnd = Random.Range(1, Letters.Count);
                let = Letters[rnd];
            } while (letters.Contains(let));

            letters.Add(let);
        }

        RandomLetters = letters;
    }

    public void OnCorrectAnswer()
    {
        AudioManager.Instance.PlayAudio("correct");

        correctAnswers++;
        correctAnswersIndicator.fillAmount = (float)correctAnswers / requiredCorrectAnswers;

        if (correctAnswers == requiredCorrectAnswers)
        {
            AudioManager.Instance.PlayAudio("clap");

            winPanel.SetActive(true);
            Instantiate(winParticlePrefab);
            Destroy(BalloonSpawner.Instance.gameObject);
        }
    }

    public void OnWrongAnswer()
    {
        AudioManager.Instance.PlayAudio("wrong");
    }
}

[System.Serializable]
public class Letter
{
    public string LetterName;
    public string LetterChar;
}