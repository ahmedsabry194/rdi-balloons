﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalloonSpawner : MonoBehaviour
{
    public GameObject[] balloonPrefabs;
    public Vector2[] balloonSpawnPositions;

    public static BalloonSpawner Instance;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);
    }

    private void Start()
    {
        StartCoroutine(SpawnBalloonsCoroutine());
    }

    private IEnumerator SpawnBalloonsCoroutine()
    {
        

        while (true)
        {
            List<int> spawnedIndexes = new List<int>();

            for (int i = 0; i < balloonSpawnPositions.Length; i++)
            {
                int targetBallonPrefabIndex = Random.Range(0, balloonPrefabs.Length);

                int spawnIndex = 0;
                do
                {
                    spawnIndex = Random.Range(0, balloonSpawnPositions.Length);
                }
                while (spawnedIndexes.Contains(spawnIndex));

                spawnedIndexes.Add(spawnIndex);

                GameObject spawnedBalloon = Instantiate(balloonPrefabs[targetBallonPrefabIndex], balloonSpawnPositions[spawnIndex], Quaternion.identity);

                yield return (new WaitForSeconds(Random.Range(2, 3)));
            }
        }
    }
}