﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneHandler : MonoBehaviour
{
    public void GoToGame()
    {
        SceneManager.LoadScene("1 - Game");
    }

    public void GoToHome()
    {
        Destroy(Balloons_LetterSelector.Instance.gameObject);

        SceneManager.LoadScene("0 - Home");
    }
}