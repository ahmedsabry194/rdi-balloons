﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class BallonLetter : MonoBehaviour
{
    public Letter Letter;

    [SerializeField] private TextMeshPro letter_TXT;

    private void Start()
    {
        this.Letter = Balloons_LetterManager.Instance.RandomLetter;
        letter_TXT.text = ArabicFixerTool.FixLine(this.Letter.LetterChar);

        letter_TXT.GetComponent<MeshRenderer>().sortingLayerName = "Balloons";
        letter_TXT.GetComponent<MeshRenderer>().sortingOrder = 1;
    }
}