﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallonMovement : MonoBehaviour
{
    public float raisingSpeed = 5;

    private void Update()
    {
        transform.position += Vector3.up * raisingSpeed * Time.deltaTime;
    }
}