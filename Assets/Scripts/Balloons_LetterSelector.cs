﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Balloons_LetterSelector : MonoBehaviour
{
    public string selectedLetter;

    public static Balloons_LetterSelector Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void SelectLetter(string letter)
    {
        this.selectedLetter = letter;
    }
}