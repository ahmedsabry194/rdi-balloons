﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class BalloonCollision : MonoBehaviour
{
    public BallonLetter ballonLetter;

    [SerializeField] private GameObject correctAnswerPrefab;
    [SerializeField] private GameObject wrongAnswerPrefab;
    [SerializeField] private Transform particleSpawnPoint;

    private void OnMouseDown()
    {
        if (ballonLetter.Letter == Balloons_LetterManager.Instance.SelectedLetter)
        {
            GameObject particle = Instantiate(correctAnswerPrefab, particleSpawnPoint.position, Quaternion.identity);
            Destroy(gameObject);
            Destroy(particle, 2);

            Balloons_LetterManager.Instance.OnCorrectAnswer();
        }
        else
        {
            GameObject particle = Instantiate(wrongAnswerPrefab, particleSpawnPoint.position, Quaternion.identity);
            Destroy(gameObject);
            Destroy(particle, 2);

            Balloons_LetterManager.Instance.OnWrongAnswer();
        }
    }
}